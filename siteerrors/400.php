<?php require_once("../includes/config.php"); render("header_foundation.php"); ?>
    <style>
		body{background-color:#eeeeee;}
		.heading{margin-top:50px;}
		.apology{color:rgba(0, 0, 0, 0.5); margin-top:25px;}
	</style>
	<div class="text-center">
      <div class="heading">
        <h1> 400 - Bad Request </h1>
      </div>
      <p class="large-4 large-offset-4 apology">
      	<strong>
       	  Sorry, the server responded with a status code of 400. <br/><br/>
          Please use the navigation panel on the top to navigate your way to the content you need. <br/><br/>
        </strong>
      </p>
      <p class="apology">
        <strong>
          If you think this is a mistake, please Email us at AnimeshSinha1309@gmail.com
        </strong>
      </p>
	</div>
  </body>
</html>