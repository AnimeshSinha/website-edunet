<?php require_once("../includes/config.php"); render("header_foundation.php"); ?>
    <style>
	  body{background-color:#eeeeee;}
	  .heading{margin-top:50px;}
	  .apology{color:rgba(0, 0, 0, 0.5); margin-top:25px;}
	</style>
	<div class="text-center">
      <div class="heading">
        <h1> 501 - Not Implemented</h1>
      </div>
      <p class="large-4 large-offset-4 apology">
      	<strong>
          We are sorry, but the website is currently under development. <br/><br/>
          The St. Thomas School website is currently the only complete feature. <a href="/schools/stthomas/">Go to St. Thomas School Website.</a>
        </strong>
      </p>
      <p class="apology">
        <strong>
          If you are a developer, you can view the current state of the website code on <a href="www.github.com/AnimeshSinha1309/Website-Edunet" target="_blank">GitHub</a>.
        </strong>
      </p>
	</div>
  </body>
</html>