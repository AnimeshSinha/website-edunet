<?php require_once("../includes/config.php"); render("header_foundation.php"); ?>
    <style>
	  body{background-color:#eeeeee;}
	  .heading{margin-top:50px;}
	  .apology{color:rgba(0, 0, 0, 0.5); margin-top:25px;}
	</style>
	<div class="text-center">
      <div class="heading">
        <h1> 403 - Forbidden </h1>
      </div>
      <p class="large-4 large-offset-4 apology">
      	<strong>
           	Sorry, but you are not allowed to access this URL. <br/><br/>
            Please use the navigation panel on the top to navigate your way to the content you need.
        </strong>
      </p>
      <p class="apology">
        <strong>
            This page will is not to be accessed by the website user.
        </strong>
      </p>
	</div>
  </body>
</html>