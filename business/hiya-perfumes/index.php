<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="The offical business page for Hiya Perfumes">
  <meta name="author" content="Animesh Sinha">
  <title>Hiya - Home</title>
  <link href="../../images/favicon.ico" rel="icon">
  <link href="../../dependencies/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../dependencies/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/landing-page.css" rel="stylesheet">
  <link href="google/fonts/fontscss.css" rel="stylesheet" type="text/css">
</head>
<body>
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Hiya International</a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php">Home</a></li>
          <li><a href="products.php">Products</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <section class="intro-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="intro-message">
            <h1>Hiya International</h1>
            <h3>An Edunet Business Advertising Page</h3>
            <hr class="intro-divider">
            <ul class="list-inline intro-social-buttons">
              <li><a href="https://www.facebook.com" class="btn btn-default btn-lg"><i class="fa fa-facebook fa-fw"></i> <span class="network-name">Facebook</span></a></li>
              <li><a href="https://plus.google.com" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a></li>
              <li><a href="https://www.linkedin.com" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="content-section-a">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 col-sm-6">
          <hr class="section-heading-spacer">
          <div class="clearfix"></div>
          <h2 class="section-heading">Our collection of Perfumes:<br>Take your Pick</h2>
          <p class="lead">We have a varied collection of scents, each one better than the other. Take what suits you best. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit, justo sit amet vulputate hendrerit, urna purus tempor tort.</p>
        </div>
        <div class="col-lg-5 col-lg-offset-2 col-sm-6">
          <img class="img-responsive" src="img/perfumes-collection.jpg" alt="">
        </div>
      </div>
    </div>
  </section>
  <section class="content-section-b">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
          <hr class="section-heading-spacer">
          <div class="clearfix"></div>
          <h2 class="section-heading">Lorem ipsum dolor<br>by sit amet</h2>
          <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit, justo sit amet vulputate hendrerit, urna purus tempor tortor, nec ultricies est ex at felis. Curabitur sed sem nunc. Nunc egestas aliquam sem, non malesuada ligula lobortis a. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
        </div>
        <div class="col-lg-5 col-sm-pull-6  col-sm-6">
          <img class="img-responsive" src="" alt="">
        </div>
      </div>
    </div>
  </section>
  <section class="content-section-a">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 col-sm-6">
          <hr class="section-heading-spacer">
          <div class="clearfix"></div>
          <h2 class="section-heading">Lorem Ipsum Dolor <br>sit amet consectetur</h2>
          <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit, justo sit amet vulputate hendrerit, urna purus tempor tortor, felis. Nunc egestas aliquam sem, non malesuada ligula lobortis a.</p>
        </div>
        <div class="col-lg-5 col-lg-offset-2 col-sm-6">
          <img class="img-responsive" src="" alt="">
        </div>
      </div>
    </div>
  </section>
  <section class="banner">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <h2>Connect to Hiya Perfumes:</h2>
        </div>
        <div class="col-lg-6">
          <ul class="list-inline banner-social-buttons">
            <li><a href="https://www.facebook.com" class="btn btn-default btn-lg"><i class="fa fa-facebook fa-fw"></i> <span class="network-name">Facebook</span></a></li>
            <li><a href="https://plus.google.com" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a></li>
            <li><a href="https://www.linkedin.com" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <ul class="list-inline">
            <li><a href="#home">Home</a></li>
            <li class="footer-menu-divider">&sdot;</li>
            <li><a href="#about">About</a></li>
            <li class="footer-menu-divider">&sdot;</li>
            <li><a href="#contact">Contact</a></li>
            <span class="list-inline navbar-right"><li><a href="/">Edunet</a></li></span>
          </ul>
          <p class="copyright text-muted small">Copyright &copy; Hiya 2014. All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>
</body>
</html>