<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="description" content="Buy products from Hiya Perfumes and Chemicals"/>
  <meta name="author" content="Animesh Sinha"/>
  <title>Hiya - Products</title>
  <script src="/dependencies/jquery/jquery-2.1.3.js"></script>
  <link href="/dependencies/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
  <link href="css/landing-page.css" rel="stylesheet"/>
  <link href="/dependencies/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link href="google/fonts/fontscss.css" rel="stylesheet" type="text/css"/>
  <link href="css/products.css" rel="stylesheet" type="text/css"/>
  <link href="../../images/favicon.ico" rel="icon">
</head>
<body>
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Hiya International</a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php">Home</a></li>
          <li><a href="products.php">Products</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <div><br/><br/><br/></div>
  <section>
    <div id="container-maincontent" class="container">
      <div class="row">
        <div class="col-lg-3 columns">
          <div class="item-wrapper">
            <div class="img-wrapper">
              <a href="../public/eventforms_cont.php?f=event_1" class="button expand add-to-cart text-center" style="width:100%; text-decoration:none;">
              	View Details and Add to Cart
              </a>
              <a href="#"><img src="img/products/perfume-1.jpg"></a>
            </div>
            <a href="#" style="text-decoration:none;"><h3>Product Name</h3></a>
            <h5>What it is, a one line description</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit, justo sit amet vulputate hendrerit, urna purus tempor tortor, nec ultricies est ex at felis. Curabitur sed sem nunc.</p>
          </div>  
        </div>
        <div class="col-lg-3 columns">
          <div class="item-wrapper">
            <div class="img-wrapper">
              <a href="../public/eventforms_cont.php?f=event_1" class="button expand add-to-cart text-center" style="width:100%; text-decoration:none;">
              	View Details and Add to Cart
              </a>
              <a href="#"><img src="img/products/perfume-1.jpg"></a>
            </div>
            <a href="#" style="text-decoration:none;"><h3>Product Name</h3></a>
            <h5>What it is, a one line description</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit, justo sit amet vulputate hendrerit, urna purus tempor tortor, nec ultricies est ex at felis. Curabitur sed sem nunc.</p>
          </div>  
        </div>
        <div class="col-lg-3 columns">
          <div class="item-wrapper">
            <div class="img-wrapper">
              <a href="../public/eventforms_cont.php?f=event_1" class="button expand add-to-cart text-center" style="width:100%; text-decoration:none;">
              	View Details and Add to Cart
              </a>
              <a href="#"><img src="img/products/perfume-1.jpg"></a>
            </div>
            <a href="#" style="text-decoration:none;"><h3>Product Name</h3></a>
            <h5>What it is, a one line description</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit, justo sit amet vulputate hendrerit, urna purus tempor tortor, nec ultricies est ex at felis. Curabitur sed sem nunc.</p>
          </div>  
        </div>
        <div class="col-lg-3 columns">
          <div class="item-wrapper">
            <div class="img-wrapper">
              <a href="../public/eventforms_cont.php?f=event_1" class="button expand add-to-cart text-center" style="width:100%; text-decoration:none;">
              	View Details and Add to Cart
              </a>
              <a href="#"><img src="img/products/perfume-1.jpg"></a>
            </div>
            <a href="#" style="text-decoration:none;"><h3>Product Name</h3></a>
            <h5>What it is, a one line description</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit, justo sit amet vulputate hendrerit, urna purus tempor tortor, nec ultricies est ex at felis. Curabitur sed sem nunc.</p>
          </div>  
        </div>
      </div>
    </div>
  </section>
  <div><br/><br/><br/></div>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <ul class="list-inline">
            <li><a href="#home">Home</a></li>
            <li class="footer-menu-divider">&sdot;</li>
            <li><a href="#about">About</a></li>
            <li class="footer-menu-divider">&sdot;</li>
            <li><a href="#contact">Contact</a></li>
            <span class="list-inline navbar-right"><li><a href="/">Edunet</a></li></span>
          </ul>
          <p class="copyright text-muted small">Copyright &copy; Hiya 2014. All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>
</body>
</html>