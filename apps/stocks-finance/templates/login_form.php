<div class="large-5 columns" style="margin-top:180px;">
	<h1>Stocks Finance</h1>
    <p> The simulator that will put your share market skills to the test, at 0 real world risk. </p>
</div>
<form action="login.php" method="post" class="large-6 columns" id="login-form">
	<div>
		<label for="login-username" class="hide">Username</label>
		<input autofocus class="form-control" name="username" placeholder="Username" id="login-username" type="text"/>
	</div>
	<div>
		<label for="login-password" class="hide">Password</label>
		<input class="form-control" name="password" placeholder="Password" id="login-password" type="password"/>
	</div>
	<div>
		<button type="submit" class="button">Log In</button>
	</div>
    <div>
        <a href="register.php">Register</a> for an account.
    </div>
</form>