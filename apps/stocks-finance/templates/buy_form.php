<form action="buy.php" method="post">
    <fieldset>
        <div class="form-group">
            <input autofocus class="form-control" name="bought" placeholder="Symbol of share" type="text"/>
        </div>
        <div class="form-group">
            <input autofocus class="form-control" name="shares" placeholder="Number of shares to buy" type="text"/>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Buy Shares</button>
        </div>
    </fieldset>
</form>