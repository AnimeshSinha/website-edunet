<!DOCTYPE html>
<html>
    <head>
        <link href="../../../dependencies/foundation/css/foundation.min.css" rel="stylesheet" type="text/css"/>
        <link href="../assets/css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="../favicon.ico" rel="icon"/>
        <?php if (isset($title)): ?><title>Stock Finance: <?= htmlspecialchars($title) ?></title>
		<?php else: ?><title>Stock Finance</title><?php endif ?>
        <script src="../assets/js/scripts.js" type="text/javascript"></script>
    </head>
    <body>
      <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
          <li class="name">
            <h1><a href="../public/">Stocks Finance</a></h1>
          </li>
        </ul>
        <section class="top-bar-section">
          <ul class="right">
            <li><a href="/">Edunet</a></li>
          </ul>
        </section>
      </nav>
      <div class="row">