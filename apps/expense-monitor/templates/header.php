<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Expense Manager</title>
  <link href="/dependencies/foundation/css/foundation.css" rel="stylesheet" type="text/css" />
  <link href="/images/favicon.ico" rel="icon" />
  <link href="/dependencies/foundation/icons/foundation-icons/foundation-icons.css" rel="stylesheet" type="text/css"/>
  <link href="/dependencies/graphing-plugins/morris/morris.css" rel="stylesheet" type="text/css">
  <script src="/dependencies/jquery/jquery-2.1.3.js"></script>
  <script src="/dependencies/graphing-plugins/morris/morris.js"></script>
  <script src="/dependencies/graphing-plugins/raphael.min.js"></script>
  <script src="/dependencies/foundation/js/foundation.min.js" type="text/javascript"></script>
  <script> $(document).ready(function(e) { $(document).foundation(); }); </script>
  <link href="../stylesheets/styles.css" rel="stylesheet" type="text/css"/>
</head>
<body class="container-fluid">
  <nav class="top-bar" data-topbar role="navigation">
    <ul class="title-area">
      <li class="name">
        <h1><a href="#">Expense Manager</a></h1>
      </li>
    </ul>
    <section class="top-bar-section">
      <ul class="right">
        <li><a href="/">Edunet</a></li>
      </ul>
    </section>
  </nav>
  <div>