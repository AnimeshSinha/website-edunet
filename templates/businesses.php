<?php render("header_foundation.php", ["title" => "Businesses", "navpos" => "businesses"]) ?>
<main>
  <div class="row" style="margin-top:50px;">
	<a href="/business/hiya-perfumes/index.php" class="large-4 columns text-center border-box">
	  <img src="../images/res-logos/logo-facebook.png" class="img-circle" width="150" height="150" />
      <h1 class="condensed">Hiya International</h1>
      <p>This part should contain a few introductary lines about the business, what its all about, be a little catchy. This should make people want to come to your page and see what you have got to say.</p>
    </a>
  </div>
</main>
<?php render("footer_foundation.php") ?>